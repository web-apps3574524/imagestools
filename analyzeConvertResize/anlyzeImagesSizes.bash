#!/bin/bash - 
#===============================================================================
#          FILE: analyseImg.bash
#         USAGE: ./analyseImg.bash 
#   DESCRIPTION: Analyse and log images with explicit weight width and height 
#  REQUIREMENTS: `identify` command from imagemagick package
#        AUTHOR: framend
#       CREATED: 25/04/2023 10:21
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -o errexit

SRC_DIR="../images/"
LOG_DIR="../images/"
LOG_FILE="$LOG_DIR/images_sizes.log"

touch $LOG_FILE

printf "Images sizes analysis started at: %s\n" "$(date +"%H:%M:%S %Y-%m-%d")" >> "$LOG_FILE"

find "$SRC_DIR" -type f \( -iname \*.jpg -o -iname \*.jpeg -o -iname \*.png -o -iname \*.webp -o -iname \*.bmp -o -iname \*.svg -o -iname \*.gif \) \
| while read -r file; do
    printf "Analyzing %s\n" "$file"
    # Get image dimensions
    dimensions=$(identify -format "%w×%h" "$file")
    # Get file name without extension
    name=$(basename -- "$file")
    name="${name%.*}"
    # Get file size in bytes
    size=$(wc -c <"$file")
    # Convert file size to human-readable format
    size_human=$(numfmt --to=iec-i --suffix=B --padding=7 "$size")
    # Write image dimensions, file size and file name to log file in three columns
    printf "%-20s %-14s %s\n" "$dimensions" "$size_human" "${SRC_DIR}${file#"${SRC_DIR}"}" >> "$LOG_FILE"
done

find "$SRC_DIR" -type f \( -iname \*.bmp -o -iname \*.jpg -o -iname \*.jpeg -o -iname \*.png -o -iname \*.webp \) -print0 | xargs -0 du -cm | awk '{s+=$1} END {print s"MB"}' >> "$LOG_FILE"


printf "Images sizes analysis completed at: %s\n" "$(date +"%H:%M:%S %Y-%m-%d")" >> "$LOG_FILE"

