#!/bin/bash
#===============================================================================
#          FILE: webpConvertAndResize.sh
#         USAGE: ./webpConvertAndResize.sh 
#  REQUIREMENTS: `convert` command from imagemagick package
#        AUTHOR: framend
#       CREATED: 24/04/2023 15:48
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -o errexit
  
SIZE_LARGE=1920
## SIZE_MEDIUM=1200
SIZE_SMALL=580

IMAGES_DIR="../images/gallery/"
OUTPUT_FORMAT="webp"
FILE_LIST=$IMAGES_DIR/images.txt

touch "$FILE_LIST"
  
find "$IMAGES_DIR" -type f \( -iname "*.png" -o -iname "*.jpg" -o -iname "*.jpeg" \) -print > "$FILE_LIST"
  
while read -r file; do
  if [[ "$file" =~ [0-9]+×[0-9]+\. ]]; then
    filename="${file%.*}"
    basefilename=$(echo "$filename" | sed -E 's/_+[0-9]+×[0-9]+$//')
  
    echo "Conversion de $file via $basefilename"
    convert "$file" -resize "$SIZE_LARGE" -quality 90 "${basefilename}_${SIZE_LARGE}.${OUTPUT_FORMAT}"
##     convert "$file" -resize "$SIZE_MEDIUM" -quality 90 "${basefilename}_${SIZE_MEDIUM}.${OUTPUT_FORMAT}"
    convert "$file" -resize "$SIZE_SMALL" -quality 90 "${basefilename}_${SIZE_SMALL}.${OUTPUT_FORMAT}"
    
    largeheight=$(identify -format "%h" "${basefilename}_${SIZE_LARGE}.${OUTPUT_FORMAT}")
##     mediumheight=$(identify -format "%h" "${basefilename}_${SIZE_MEDIUM}.${OUTPUT_FORMAT}")
    smallheight=$(identify -format "%h" "${basefilename}_${SIZE_SMALL}.${OUTPUT_FORMAT}")
 
    mv "${basefilename}_${SIZE_LARGE}.${OUTPUT_FORMAT}" "${basefilename}_${SIZE_LARGE}×${largeheight}.${OUTPUT_FORMAT}"
##     mv "${basefilename}_${SIZE_MEDIUM}.${OUTPUT_FORMAT}" "${basefilename}_${SIZE_MEDIUM}×${mediumheight}.${OUTPUT_FORMAT}"
    mv "${basefilename}_${SIZE_SMALL}.${OUTPUT_FORMAT}" "${basefilename}_${SIZE_SMALL}×${smallheight}.${OUTPUT_FORMAT}"
  fi
 
done < "$FILE_LIST"
  
rm "$FILE_LIST"
 
