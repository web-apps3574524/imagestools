#!/bin/bash - 
#===============================================================================
#          FILE: copyAndRenameWithSizes.bash
#         USAGE: ./copyAndRename.sh 
#   DESCRIPTION: Add a suffix with sizes in pixels to images files
#  REQUIREMENTS: `identify` command from imagemagick package
#        AUTHOR: framend
#       CREATED: 25/04/2023 11:41
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -o errexit

SRC_DIR="../images"

find "$SRC_DIR" -type f \( -iname \*.jpg -o -iname \*.jpeg -o -iname \*.png -o -iname \*.webp -o -iname \*.bmp -o -iname \*.svg -o -iname \*.gif \) \
| while read -r file; do
    dimensions=$(identify -format "%w %h" "$file")
    
    if [[ ! "$file" =~ [0-9]+×[0-9]+\. ]]; then
        new_name="${file%.*}_${dimensions// /×}.${file##*.}"
        if [ -f "$new_name" ]; then
            printf "Skipping '%s' because the target file already exists\n" "$file"
        else
            cp "$file" "$new_name"
            printf "Copied '%s' to '%s'\n" "$file" "$new_name"
        fi
    else
        printf "Skipping '%s' because it already has the correct filename format\n" "$file"
    fi
done

